<?php
include_once ('com/celebration/domain/User.php');
include_once ('com/celebration/domain/SearchResult.php');
include_once ('com/celebration/domain/SearchCriteria.php');
include_once ('com/celebration/controller/SearchController.php');

$searchResult = new SearchResult();
session_start();

if (!isset($_SESSION["SEARCH_RESULT"]) && isset($_SESSION[User::USR_ID])){
	
	$searchCriteria = new SearchCriteria();
	$searchCriteria->setUserId($_SESSION[User::USR_ID]);
	$searchResult = SearchController::getInstance()->search($searchCriteria);
	$_SESSION["SEARCH_RESULT"] = $searchResult;
}

include 'editUser.php';

?>