<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Review and rating about Marriage Halls, Catering Services and
	photographer</title>
<meta name="description"
	content="Review and rating about Marriage Halls, Catering Services and photographer." />
<meta name="keywords" content="Zen Template" />
<link rel="stylesheet" type="text/css"  href="css/style.css" />
<link rel="stylesheet" type="text/css"  href="css/search.css">
<link rel="stylesheet" type="text/css"  href="css/textField.css">
<link rel="stylesheet" type="text/css"  href="css/checkBox.css">
<link rel="stylesheet" type="text/css"  href="css/notify.css">
</head>
<body>

<?php 
	if (!isset($_SESSION)) 
	{ 
		session_start();
	}
?>
	<!-- Paste Facebook LikeBox Javascript Here -->
	<div class="wrap">
		<div class="header">
			<div class="topbar">
				<div class="social">
					<a href="#" target="_blank" title="twitter" class="twitter">twitter</a>
					<a href="#" target="_blank" title="facebook" class="facebook">facebook</a>
					<a href="#" target="_blank" title="linkedin" class="linkedin">linkedin</a>
				</div>
	
				<!--close social -->
				<div class="phone">Tel: 0345 517 4084</div>
				<!-- ############################ Menu ###################################### -->
				<?php
					if (isset($_SESSION["USR_ID"])){
						include("private-menu.php");
					}else{
						include("public-menu.php");
					}
				?>
				
				
				<!-- close menu -->
			</div>
			<!--close topbar -->
			<div class="blacktab">
				<div class="logo">
					<a href="#"><img src="images/logo1.png" alt="Zen Template" /></a>
				</div>
			</div>
			<div class="banner">
				<img src="images/banner.jpg" height="240" width="880"
					alt="Zen Banner" />
			</div>
		</div>
		<!-- close header -->
		<div class="page">
		<?php include("notify.php"); ?>