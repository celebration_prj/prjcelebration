<?php

//activationProcessor.php

include_once ('com/celebration/domain/User.php');
include_once ('com/celebration/domain/ResponseEntity.php');
include_once ('com/celebration/controller/UserController.php');
include_once ('com/celebration/controller/EmailController.php');

$user = new User();
$response = new  ResponseEntity();

$user->setUserName($_POST[User::USR_NAME]);
$user->setActivationCode($_POST[User::ACT_CODE]);

$response = UserController::getInstance ()->activateUserByUserNameAndActivationCode($user);
$user = $response->getEntity();

/*Taking Care of Session to send notify messages back to the page*/
if(!isset($_SESSION)) {
	session_start();
}
unset($_SESSION['ERROR_MESSAGES']);
unset($_SESSION['SUCCESS_MESSAGE']);

if ($response->hasError()){
	$_SESSION['ERROR_MESSAGES'] = $response->getErrorMessages();
	include('activate.php');

}else{
	$successMessage = array();
	$successMessage[] =  "User ID : " . $user->getFname() . " " . $user->getLname() . " activate successfully.";
	$_SESSION['SUCCESS_MESSAGE'] = $successMessage;
	include('success.php');
}

?>