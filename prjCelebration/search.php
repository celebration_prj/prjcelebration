<?php include("header.php");?>
<div class="main">
	<!-- ************************************** Form ******************************************** -->
	<form>
		<div id="searchCriteriaSection">
			<h2>Search Business</h2>
			<table>
				<tbody>
					<tr>
						<td>First Name :</td>
						<td><input type="text" id="firstName"></td>
						<td>Last Name :</td>
						<td><input type="text" id="lastName" class="simple-input"></td>
					</tr>
					<tr>
						<td>Business Name :</td>
						<td><input type="text" id="businessName"></td>
						<td>Business Website :</td>
						<td><input type="text" id="businessWebsite"
							placeholder="www.abc.com" pattern="www.+"></td>
					</tr>
					<tr>
					
					
					<tr>
						<td colspan="2"><h4>Business Services :</h4></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="checkbox" name="businessServices"
							value="catering" id="catering" checked="checked">Catering</td>
						<td><input type="checkbox" name="businessServices"
							value="photographer" id="photographer">Photographer</td>
						<td><input type="checkbox" name="businessServices"
							value="banquetHall" id="banquetHall">Banquet Hall</td>
					</tr>

					<tr>
						<td>City:</td>
						<td><input type="text" id="city"></td>
						<td>State / Province / Region:</td>
						<td><input type="text" id="stateProvinceRegion"></td>
					</tr>
					<tr>
						<td>Postal / Zip Code:</td>
						<td><input type="text" id="postalZipCode"></td>
						<td>Country:</td>
						<td><select id="Country242" class="">
								<option value="pakistan">Pakistan</option>
								<option value="Afghanistan">Afghanistan</option>
						</select></td>
					</tr>
					<tr>
						<td colspan="4" align="right"><button id="search">Search</button></td>
					</tr>
				</tbody>
			</table>
		</div>
		<!--  end "searchCriteriaSection" section-->

		<div class="container">
			<div class="wrapper">
				<div class="scroll-wrapper">

					<div id="searchResultSection">
						<table id="resultTable" class="resultTable" cellspacing="0" style="width: 100%;">
							<!--  <caption>Business search result.</caption>  -->
							<tr>
								<th scope="col" abbr="Configurations">Business Name</th>
								<th scope="col" abbr="Dual 1.8GHz">Services</th>
								<th scope="col" abbr="Dual 2GHz">City</th>
								<th scope="col" abbr="Dual 2.5GHz">Contact Number</th>
								<th scope="col" abbr="Dual 2.5GHz">Email Address</th>
							</tr>
							<tr>
								<td>Model</td>
								<td>M9454LL/A</td>
								<td>M9455LL/A</td>
								<td>M9457LL/A</td>
								<td>M9457LL/A</td>
							</tr>
							<tr>
								<td>Model</td>
								<td>M9454LL/A</td>
								<td>M9455LL/A</td>
								<td>M9457LL/A</td>
								<td>M9457LL/A</td>
							</tr>
							<tr>
								<td>Model</td>
								<td>M9454LL/A</td>
								<td>M9455LL/A</td>
								<td>M9457LL/A</td>
								<td>M9457LL/A</td>
							</tr>
							<tr>
								<td>Model</td>
								<td>M9454LL/A</td>
								<td>M9455LL/A</td>
								<td>M9457LL/A</td>
								<td>M9457LL/A</td>
							</tr>																					
						</table>
					</div>
					<!--  end "searchResultSection" section-->
				</div>
			</div>
		</div>

		<!-- http://veerle-v2.duoh.com/blog/comments/a_css_styled_table/
			<table id="mytable" cellspacing="0" summary="The technical specifications of the Apple PowerMac G5 series">
			   <caption>Table 1: Power Mac G5 tech specs </caption>
			   <tr>
				  <th scope="col" abbr="Configurations" class="nobg">Configurations</th>
				  <th scope="col" abbr="Dual 1.8GHz">Dual 1.8GHz</th>
				  <th scope="col" abbr="Dual 2GHz">Dual 2GHz</th>
				  <th scope="col" abbr="Dual 2.5GHz">Dual 2GHz</th>
			   </tr>
			   <tr>
				  <th scope="row" class="spec">Model</th>
				  <td>M9454LL/A</td>
				  <td>M9455LL/A</td>
				  <td>M9457LL/A</td>
			   </tr>
			</table>
			-->
	</form>

	<!-- ********************************************* FORM END ************************* -->
</div>
<?php include("footer.php");?>						