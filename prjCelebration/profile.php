<?php include("header.php");?>
<div class="main">
	<!-- ************************************** Form ******************************************** -->
	<?php
		$searchResult = new SearchResult();
		$searchResult = $_SESSION["SEARCH_RESULT"];
	?>
	<form method="post" action="profileProcessor.php">
         <h2>Business Profile Registration</h2>  
         <table>
			<tbody>
				<tr>
					<td><input type="hidden" id="ADD_ID" 	  name="ADD_ID"       value="<?php echo $searchResult->getAddressId(); ?>" /></td>
					<td><input type="hidden" id="BUS_PRFL_ID" name="BUS_PRFL_ID"  value="<?php echo $searchResult->getBusinessProfileId(); ?>" /></td>
				</tr>			
				<tr>
					<td>First Name :</td>
					<td><input type="text" id="BUS_PRFL_FNAME" name ="BUS_PRFL_FNAME" value="<?php echo $searchResult->getBusinessFName(); ?>" required></td>
					<td>Last Name :</td>
					<td><input type="text" id="BUS_PRFL_LNAME" name ="BUS_PRFL_LNAME"  value="<?php echo $searchResult->getBusinessLName(); ?>"  required></td>
				</tr>
				<tr>
					<td>Business Name :</td> 
					<td><input type="text" id="BUS_PRFL_NAME" name="BUS_PRFL_NAME"  value="<?php echo $searchResult->getBusinessName(); ?>" required></td>
					<td>Business Website :</td>
					<td><input type="text" id="BUS_PRFL_WEB" name="BUS_PRFL_WEB"  value="<?php echo $searchResult->getBusinessWebsite(); ?>" pattern="www.+"></td>
				</tr>
				<tr>
					<td colspan="2"><h4>Business Services :</h4></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="checkbox" name="SRVS_TYPE_3" value="3" id="SRVS_TYPE_3" checked="checked">Catering</td>
					<td><input type="checkbox" name="SRVS_TYPE_2" value="2" id="SRVS_TYPE_2">Photographer</td>
					<td><input type="checkbox" name="SRVS_TYPE_1" value="1" id="SRVS_TYPE_1">Banquet Hall</td>
				</tr>
				<!-- 
				<tr>
					<td colspan="2"><h4>Business Activation :</h4></td>
				</tr>

				<tr>
					<td></td>
					<td><input type="radio" name="active" id="active" checked="checked">Active</td>
					<td><input type ="radio" name="inactive" id="inactive">Inactive</td>
				</tr>
				-->
				<tr>
					<td>Business Email :</td>
					<td><input type="text" id="BUS_PRFL_EMAIL" name="BUS_PRFL_EMAIL" value="<?php echo $searchResult->getBusinessEmail(); ?>"></td>
					<td>Business Phone :</td>
					<td><input type="text" id="BUS_PRFL_PHONE" name="BUS_PRFL_PHONE"  value="<?php echo $searchResult->getBusinessPhone(); ?>" ></td>
				</tr>

				<tr>
					<td colspan="2">
						<h4>Business Address</h4>
					</td>
				</tr>
				<tr>
					<td>Address Line 1:</td>
					<td colspan="3"><input type="text" id="ADD_LINE_1"  name="ADD_LINE_1" value="<?php echo $searchResult->getAddressLine1(); ?>" size="87" value=""></td>
				</tr>
				<tr>
					<td>Address Line 2:</td>
					<td colspan="3"><input type="text" id="ADD_LINE_2"  name="ADD_LINE_2"  value="<?php echo $searchResult->getAddressLine2(); ?>" size="87"></td>
				</tr>
				<tr>
					<td>City:</td>
					<td><input type="text" id="CITY" name="CITY" value="<?php echo $searchResult->getCity(); ?>"></td>
					<td>State / Province / Region:</td>
					<td><input type="text" id="STATE" name="STATE" value="<?php echo $searchResult->getState(); ?>"></td>
				</tr>
				<tr>
					<td>Postal / Zip Code:</td>
					<td><input type="text" id="ZIP" name="ZIP" value="<?php echo $searchResult->getZip(); ?>"></td>
					<td>Country:</td>
					<td><select id="Country" class="" value="<?php echo $searchResult->getCountry(); ?>">
							<option value="pakistan">Pakistan</option>
							<option value="Afghanistan">Afghanistan</option>
					</select></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td><button id="register">Register</button></td>
				</tr>
			</tbody>
		</table>
	</form>
	<!-- ********************************************* FORM END ************************* -->
	<?php unset( $_SESSION["SEARCH_RESULT"]); ?>
</div>
<?php include("footer.php");?>			