<?php include("header.php");?>
<div class="main">
	<?php
		$searchResult = new SearchResult();
		$searchResult = $_SESSION["SEARCH_RESULT"];
	?>
	<!-- ************************************** Form ******************************************** -->
	<form method="post" action="userProcessor.php">
         <h2>User  Information</h2>
		<table>
			<tbody>
				<tr>
					<td><input type="hidden" id="USR_ID" name="USR_ID"  value="<?php echo $searchResult->getUserId(); ?>"></td>
				</tr>			
				<tr>
					<td>First Name :</td>
					<td><input type="text" id="USR_FNAME" name="USR_FNAME"  value="<?php echo $searchResult->getFname(); ?>"></td>
				</tr>
				<tr>
					<td>Last Name :</td>
					<td><input type="text" id="USR_LNAME"  name="USR_LNAME"   value="<?php echo $searchResult->getLname(); ?>"></td>
				</tr>
				<tr>
					<td>Login ID :</td>
					<td><input type="text" id="USR_NAME"  name="USR_NAME"  value="<?php echo $searchResult->getUserName(); ?>" disabled="disabled"></td>
				</tr>
				<tr>
					<td>Email :</td>
					<td><input type="text" id="EMAIL"  name="EMAIL"  value="<?php echo $searchResult->getPersonalEmail(); ?>"  disabled="disabled"></td>
				</tr>
				<tr>
					<td>Phone :</td>
					<td><input type="text" id="PHONE" name="PHONE"  value="<?php echo $searchResult->getPersonalPhone(); ?>"></td>
				</tr>										
				<tr>
					<td colspan="2" align="right"><button id="register">Save</button></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>
	<!-- ********************************************* FORM END ************************* -->
<?php include("footer.php");?>						