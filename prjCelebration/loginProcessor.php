<?php

include_once ('com/celebration/domain/User.php');
include_once ('com/celebration/domain/ResponseEntity.php');
include_once ('com/celebration/controller/UserController.php');
include_once ('com/celebration/controller/EmailController.php');

$user = new User();
$response = new  ResponseEntity();

$user->setUserName($_POST[User::USR_NAME]);
$user->setUserPassword($_POST[User::USR_PSWD]);

$response = UserController::getInstance ()->validateUserLogin($user);
$user = $response->getEntity();

/*Taking Care of Session to send notify messages back to the page*/
if(!isset($_SESSION)) {
	session_start();
}
unset($_SESSION['ERROR_MESSAGES']);
unset($_SESSION['SUCCESS_MESSAGE']);

if ($response->hasError()){
	$_SESSION['ERROR_MESSAGES'] = $response->getErrorMessages();
	include('user.php');

}else{
	$successMessage = array();
	$successMessage[] =  "User " . $user->getFname() . " " . $user->getLname() . " Login successfully.";
	$_SESSION['SUCCESS_MESSAGE'] = $successMessage;
	loadUserToSession($user);
	include('success.php');
}

function loadUserToSession(User $user){
	$_SESSION[User::USR_ID] = $user->getId();
	$_SESSION[User::USR_NAME] = $user->getUserName();
	$_SESSION[User::USR_FNAME] = $user->getFname();
	$_SESSION[User::USR_LNAME] = $user->getLName();
	
}

?>