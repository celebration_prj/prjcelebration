<?php

include ('com/celebration/domain/User.php');
include ('com/celebration/domain/SearchResult.php');
include ('com/celebration/domain/ResponseEntity.php');
include ('com/celebration/controller/UserController.php');
include ('com/celebration/controller/EmailController.php');

session_start();
$user = new User();

$user->setFname($_POST[User::USR_FNAME]);
$user->setLname($_POST[User::USR_LNAME]);
$user->setPersonalEmail($_POST[User::EMAIL]);
$user->setPersonalPhone($_POST[User::PHONE]);

if (isset($_POST[User::USR_ID]) && !empty($_POST[User::USR_ID])){
	$user->setId($_POST[User::USR_ID]);
	UserController::getInstance ()->update($user);
	
	$searchResult = new SearchResult();
	$searchResult = $_SESSION["SEARCH_RESULT"];
	
	$_SESSION["SEARCH_RESULT"]->setFname($user->getFname());
	$_SESSION["SEARCH_RESULT"]->setLname($user->getLname());
	$_SESSION["SEARCH_RESULT"]->setPersonalPhone($user->getPersonalPhone());
	
	
	print " User update success fully ....! ";
	
}else{

	//Getting activation code to add to user to register.
	$activationCode = strtoupper(uniqid());
	
	$user->setUserPassword($_POST[User::USR_PSWD]);
	$user->setUserName($_POST[User::USR_NAME]);
	//Load Default values for new record in the database
	$user->loadDefaultVersionAndActiveIndicator();
	// Set User inActive because  it has to been verified first.
	$user->setIsActive("0");
	$user->setCreatedBy($_POST[User::USR_NAME]);
	$user->setUpdatedBy($_POST[User::USR_NAME]);
	$user->setUpdatedBy($_POST[User::EMAIL]);
	$user->setActivationCode($activationCode);
	
	$response = new ResponseEntity();
	$response = UserController::getInstance ()->save($user);

	/*Taking Care of Session to send notify messages back to the page*/
	if(!isset($_SESSION)) {
		session_start();
	}
	unset($_SESSION['ERROR_MESSAGES']);
	unset($_SESSION['SUCCESS_MESSAGE']);
	
	if ($response->hasError()){
		$_SESSION['ERROR_MESSAGES'] = $response->getErrorMessages();
		include('user.php');
	
	}else{
		$successMessage = array();
		$successMessage[] = "User has been register successfully." . 
							" An activation email has been sent to your e	mail address, Please check your Junk/Spam folder.";
		$_SESSION['SUCCESS_MESSAGE'] = $successMessage;
		
		// When User registered then Lets send activation code email to user. 
		if(EmailController::getInstance()->sentEmail($user->getPersonalEmail(), $activationCode , $user->getUserName())){
			include('success.php');
		}else{
			include('index.php');
		}
		
		
	}
}

?>