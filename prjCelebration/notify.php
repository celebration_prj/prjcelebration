
<div>

	
	<?php
	if(isset($_SESSION)){

		// ERROR MESSAGE
		if(!empty($_SESSION['ERROR_MESSAGES'])) {
			echo '<div class="notify notify-red">';
			foreach ($_SESSION['ERROR_MESSAGES'] as $errorMessage){
				echo '<span class="symbol icon-error"></span><p>'. $errorMessage . '</p><br>';
			}
			echo '</div>';
		}
		// SUCCESS MESSAGE
		if(!empty($_SESSION['SUCCESS_MESSAGE'])) {
			echo '<div class="notify notify-green">';
			foreach ($_SESSION['SUCCESS_MESSAGE'] as $successMessage){
				echo '<span class="symbol icon-tick"></span><p>'. $successMessage . '</p><br>';
			}
			echo '</div>';
		}
		
		unset($_SESSION['ERROR_MESSAGES']);
		unset($_SESSION['SUCCESS_MESSAGE']);
		
	}

	?>
 
</div>			