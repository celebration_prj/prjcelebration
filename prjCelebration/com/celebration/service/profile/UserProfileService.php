<?php
include_once ('com/celebration/dao/profile/UserProfileDAO.php');

class UserProfileService{

	static private $instance = null;

	private function __construct() {
		
	}

	static public function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new self();
		}
		return self::$instance;
	}


	public function save(UserProfile $userProfile){
		return UserProfileDAO::getInstance()->save($userProfile);
	}
	public function update(UserProfile $userProfile){
		return UserProfileDAO::getInstance()->update($userProfile);
	}
	


}

?>