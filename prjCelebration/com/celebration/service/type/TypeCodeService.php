<?php
include_once ('com/celebration/dao/type/TypeCodeDAO.php');

class UserProfileService{

	static private $instance = null;

	private function __construct() {

	}

	static public function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function saveServiceTypeCode(ServiceTypeCode $typCode){
		TypeCodeDAO::getInstance()->saveServiceTypeCode($typCode);
	}


}

?>