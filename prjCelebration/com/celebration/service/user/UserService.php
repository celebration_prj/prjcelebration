<?php

include ('com/celebration/dao/user/UserDAO.php');

class UserService{
	
	static private $instance = null;
	private $userDAO;
	
	
	private function __construct() {
		$this->userDAO = UserDAO::getInstance();
	}

	static public function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function getUser($paramArray) { 
		return $this->userDAO->getUser($paramArray);
	}
	
	public function getUserByLoginCredential(User $user){
		return $this->userDAO->validateUserLogin($user);
	}
	
	public function getUserByEmailOrUserName($userEmail , $userName){
		return $this->userDAO->getUserByEmailOrUserName($userEmail, $userName);
	}
	
	public function getUserByUserNameAndActivationCode(User $user){
		return $this->userDAO->getUserByUserNameAndActivationCode($user);
	}

	public function activateUser(User $user){
		return $this->userDAO->activateUser($user);
	}
	
	public function save(User $user) {
		$this->userDAO->save($user);
	}
	
	public function update(User $user) { 
		$this->userDAO->update($user);
	}
	
	public function deleteUser(User $user) { 
		$this->userDAO->deleteUser($user);
	}
	
}

?>