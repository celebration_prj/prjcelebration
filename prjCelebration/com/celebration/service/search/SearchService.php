<?php

include_once 'com/celebration/dao/search/SearchDAO.php';

class SearchService{

	static private $instance = null;

	private function __construct() {
		$this->searchDAO = SearchDAO::getInstance();
	}

	static public function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	public function search(SearchCriteria $searchCriteria) {
		return SearchDAO::getInstance()->search($searchCriteria);
	}
}
?>

