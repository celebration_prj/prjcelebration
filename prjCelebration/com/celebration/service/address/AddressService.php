<?php
include_once ('com/celebration/dao/address/AddressDAO.php');

class AddressService{

	static private $instance = null;

	private function __construct() { }

	static public function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function save(Address $address){
		return AddressDAO::getInstance()->save($address);
	}
	
	public function update(Address $address){
		AddressDAO::getInstance()->update($address);
	}

}

?>