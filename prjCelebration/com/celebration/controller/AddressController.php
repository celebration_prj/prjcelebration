<?php

include_once ('com/celebration/service/address/AddressService.php');


class AddressController {

	private static $instance = null;

	private function __construct() { }

	static public function getInstance() {
		if (self::$instance === null) {
			self::$instance = new self ();
		}
		return self::$instance;
	}

	public function save(Address $address){
		return AddressService::getInstance()->save($address);
	}
	public function update(Address $address){
		AddressService::getInstance()->update($address);
	}
	

}
?>