<?php

include_once ('com/celebration/service/type/TypeCodeService.php');


class AddressCodeController {

	private static $instance = null;

	private function __construct() { }

	static public function getInstance() {
		if (self::$instance === null) {
			self::$instance = new self ();
		}
		return self::$instance;
	}

	public function saveServiceTypeCode(ServiceTypeCode $typCode){
		TypeCodeService::getInstance()->saveServiceTypeCode($typCode);
	}


}
?>