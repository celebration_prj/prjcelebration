<?php

include_once ('com/celebration/service/search/SearchService.php');

class SearchController {
	
	private static $instance = null;

	private function __construct() {
		$this->searchService = SearchService::getInstance();
	}

	static public function getInstance() {
		if (self::$instance === null) {
			self::$instance = new self ();
		}
		return self::$instance;
	}

	public function search(SearchCriteria $searchCriteria) {
		return SearchService::getInstance()->search($searchCriteria);
	}
	
}
?>