<?php
class EmailController {
	private static $instance = null;
	private function __construct() {
	}
	static public function getInstance() {
		if (self::$instance === null) {
			self::$instance = new self ();
		}
		
		return self::$instance;
	}
	public function sentEmail($to, $code, $userId) {
		
		$activationLink = "http://192.168.1.105/prjCelebration/activate.php?user=" . $userId . "&code=" . $code ;
		
		$toEmail = "celebration.prj@gmail.com";
		$from = "celebration.prj@gmail.com";
		$subject = "Verification Code...!";
		$body = "<p>You recently added " . 
				$to . " the register email address for your Celebration ID. To verify this email address belongs to you, enter the code below on the email verification page:</p>" . 
				'Visit <a href="http://192.168.1.105/prjCelebration/activate.php"> activate user page </a>and enter user id with this activation code <b>' . $code . '</b> to activate your account.'.
				' or Click on following <a href="' . $activationLink . '">Activate</a> link to activate your account.' .
				"<p><b>Why you received this email.</b></p><p>Celebration requires verification whenever an email address is selected for your Celebration ID. Your email cannot be used until you verify it. <br>If you did not make this change or you believe an unauthorized person has accessed your account, you should change your password as soon as possible from your Celebration account.</p>";
		
		// add headers
		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'To: celebration.prj@gmail.com' . "\r\n";
		$headers .= 'From: celebration.prj@gmail.com' . "\r\n";
		
		return mail ( $toEmail, $subject, $body, $headers );
	}
	private function loadEmail() {
	}
}
?>

