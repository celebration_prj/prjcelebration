<?php

include_once ('com/celebration/service/profile/UserProfileService.php');


class UserProfileController{

	static private $instance = null;

	private function __construct() {}


	static public function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function save(UserProfile $userProfile){
		return UserProfileService::getInstance()->save($userProfile);
	}
	
	public function update(UserProfile $userProfile){
		return UserProfileService::getInstance()->update($userProfile);
	}
	
}

?>