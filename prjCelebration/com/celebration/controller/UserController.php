<?php

include ('com/celebration/service/user/UserService.php');
include ('com/celebration/rules/UserRules.php');

class UserController{
	
	static private $instance = null;

	private function __construct() { }
	
	/*Method : a way to implement singleton object.*/
	static public function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function validateUserLogin(User $user){
		
		$response = new  ResponseEntity();		
		$response->setErrorMessages (UserRules::getInstance ()->validateUserInformationToLogin($user));
		
		if (!$response->hasError ()) {
			$response->setEntity(UserService::getInstance()->getUserByLoginCredential($user));
			$response->setErrorMessages (UserRules::getInstance ()->validateLogin($response->getEntity()));
		}

		return $response;
	}
	
	public function getUser($paramArray) {
		
		$response = new  ResponseEntity();
		$response->setEntity(UserService::getInstance()->getUser($paramArray));
		return $response ;
	}
	/**
	 * 
	 * @param unknown $userName
	 * @param unknown $activationCode
	 * @return ResponseEntity
	 */
	public function activateUserByUserNameAndActivationCode(User $userArg){
		$response = new  ResponseEntity();
		$response->setErrorMessages (UserRules::getInstance ()->validateUserToActivate ($userArg) );

		if (!$response->hasError ()) {
			$user = new User();
			$user = UserService::getInstance()->getUserByUserNameAndActivationCode($userArg);
						
			$response->setErrorMessages ( UserRules::getInstance ()->validateExistingUserToActivate($user));
		}
		
		/* If no user attached to the requested activate, send error message back*/
		if($response->hasError()){
			$response->setEntity(null);
			return $response;
		}
		
		UserService::getInstance()->activateUser($user);
		$response->setEntity($user);
		return $response;
		
	}
	
	public function save(User $user) {
		
	   $response = new  ResponseEntity();
	   $response->setErrorMessages (UserRules::getInstance ()->validateNewUserRegistration ( $user ) );
	   
		if (!$response->hasError ()) {
			$response->setErrorMessages ( UserRules::getInstance ()->validateExistingUser ( UserService::getInstance()->getUserByEmailOrUserName ( $user->getPersonalEmail (), $user->getUserName () ) ) );
		}
	    
		/* If any of the validation error exisit then Will send error message back to client.*/
	   if($response->hasError()){
	   	$response->setEntity(null);
	   	return $response;
	   }
	   
	   $response->setEntity(UserService::getInstance()->save($user));
	   return $response;
	}
	
	public function update(User $user) {
		UserService::getInstance()->update($user);
	}
	
	public function deleteUser(User $user) {
		UserService::getInstance()->deleteUser($user);
	}
	
}

?>