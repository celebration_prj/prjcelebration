<?php
class SearchTransformer{

	private static $instance = null;
	
	private function __construct() { }
	
	static public function getInstance() {
	
		if (self::$instance === null) {
			self::$instance = new self ();
		}
		return self::$instance;
	}
	
	
	public function loadSearchResultFromResultSet($result){
	
		$searchResult = new SearchResult();
	
		if(!isset($result) or is_null($result)){
			return $user;
		}
	
		/* Business Profile Related Fields*/
		$searchResult->setUserId($result [SearchResult::USR_ID]);
		$searchResult->setFname($result [SearchResult::USR_FNAME]);
		$searchResult->setLname($result [SearchResult::USR_LNAME]);
		//$searchResult->setUserType($result [SearchResult::USR_]);
		$searchResult->setPersonalEmail($result [SearchResult::EMAIL]);
		$searchResult->setPersonalPhone($result [SearchResult::PHONE]);
		$searchResult->setUserName($result [SearchResult::USR_NAME]);
		$searchResult->setUserPassword($result [SearchResult::USR_PSWD]);
		
		/* Business Profile Related Fields*/
		$searchResult->setBusinessProfileId($result [SearchResult::BUS_PRFL_ID]);
		$searchResult->setBusinessFName($result [SearchResult::BUS_PRFL_FNAME]);
		$searchResult->setBusinessLName($result [SearchResult::BUS_PRFL_LNAME]);
		$searchResult->setBusinessName($result [SearchResult::BUS_PRFL_NAME]);
		$searchResult->setBusinessWebsite($result [SearchResult::BUS_PRFL_WEB]);
		$searchResult->setBusinessPhone($result [SearchResult::BUS_PRFL_PHONE]);
		$searchResult->setBusinessEmail($result [SearchResult::BUS_PRFL_EMAIL]);
		
		
		$searchResult->setAddressId($result [SearchResult::ADD_ID]);
		$searchResult->setAddressLine1($result [SearchResult::ADD_LINE_1]);
		$searchResult->setAddressLine2($result [SearchResult::ADD_LINE_2]);
		$searchResult->setCity($result [SearchResult::CITY]);
		$searchResult->setState($result [SearchResult::STATE]);
		$searchResult->setZip($result [SearchResult::ZIP]);
		$searchResult->setCountry($result [SearchResult::CNTRY]);
	
		return $searchResult;
	
	}
	
}
?>