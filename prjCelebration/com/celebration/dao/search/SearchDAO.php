<?php

include_once ('com/celebration/dao/db/DBConnection.php');
include_once ('com/celebration/dao/search/SearchTransformer.php');

class SearchDAO {

	private static $instance = null;
	const SELECT = " SELECT  ";
	const USR_COL_WITH_U = " U.USR_ID, U.USR_FNAME, U.USR_LNAME, U.USR_NAME, U.USR_PSWD, U.EMAIL, U.PHONE ";
	const BUS_PRFL_COL_WITH_BP = " , BP.BUS_PRFL_ID, BP.BUS_PRFL_FNAME, BP.BUS_PRFL_LNAME, BP.BUS_PRFL_NAME, BP.BUS_PRFL_WEB, BP.BUS_PRFL_EMAIL, BP.BUS_PRFL_PHONE ";		
	const ADDRESS_COL_WITH_A = " , A.ADD_ID, A.ADD_LINE_1, A.ADD_LINE_2, A.CITY, A.STATE, A.ZIP, A.CNTRY " ;
	const FROM = " FROM " ;
	const TBL_USR_WITH_U = " USR U ";
	const IN_JOIN_BUS_PRFL_WITH_USR = " LEFT JOIN BUS_PRFL BP ON BP.USR_ID = U.USR_ID ";
	const IN_JOIN_ADD_WITH_BUS_PRFL = " LEFT JOIN ADDRESS A ON BP.BUS_PRFL_ID = A.BUS_PRFL_ID ";
	const WHERE = " WHERE " ;
	const USER_ID_IS_EQUAL_TO = " U.USR_ID = ?" ;

	private function __construct() {
	}

	static public function getInstance() {
		if (self::$instance === null) {
			self::$instance = new self ();
		}
		return self::$instance;
	}

	public function search(SearchCriteria $searchCriteria) { 
		$query = "";
		$arrayArgument = array();
		
		$query = $this->getQuery();
		
		if(!is_null($searchCriteria->getUserId()) && !empty($searchCriteria->getUserId())){
			// Adding where caluse
			$query = $query . SearchDAO::USER_ID_IS_EQUAL_TO;
			// Adding Value to where caluse
			$arrayArgument[] = $searchCriteria->getUserId();
 		}
		
 	
 		$searchResult = new SearchResult();
 		 		
 		$result =  DBConnection::getInstance()->executeSQLQuery ( $query ,$arrayArgument);
 		$searchResult = SearchTransformer::getInstance()->loadSearchResultFromResultSet($result);
 		
 		return $searchResult;
	
	}
	
	public function getUsrAndBusProfileAndAddressByUserId($userId){
		$query = null;
		$arrayArgument = array();
		buildQuery($searchCriteria,$arrayArgument,$query);
		
// 		$result =  DBConnection::getInstance()->executeSQLQuery ( getQuery() ,array($userId));
		
// 		if(is_array($result)){
// 			$user = UserTransformer::getInstance()->loadUserFromResultSet($result);
// 		}
		
// 		return $user;
		
	}
	private function getQuery(){
		return SearchDAO::SELECT . SearchDAO::USR_COL_WITH_U . SearchDAO::BUS_PRFL_COL_WITH_BP . SearchDAO::ADDRESS_COL_WITH_A .
		SearchDAO::FROM . SearchDAO::TBL_USR_WITH_U . SearchDAO::IN_JOIN_BUS_PRFL_WITH_USR . SearchDAO::IN_JOIN_ADD_WITH_BUS_PRFL . 
		SearchDAO::WHERE ;
	}
	
}
?>