<?php

include_once 'com/celebration/utility/DBProperties.php';

class DBConnection{

	private static $instance = null;

	private function __construct() {}

	static public function getInstance() {
		if (self::$instance === null) {
			self::$instance = new self ();
		}
		return self::$instance;
	}

	
	public function __destruct() {}

	 public function executeSQLQuery($query,$paramArray) { 
		try {
			
			$dsn = "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=" . CHAR_SET;
			$opt = [ PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_EMULATE_PREPARES => false ];
			$pdo = new PDO ( $dsn, DB_USER, DB_PASSWORD, $opt );
			
			/*
			print 'executeSQLQuery : ' . $query;
			print_r ($paramArray);
			*/
			
			$stmt = $pdo->prepare($query);
			$stmt->setFetchMode(PDO::FETCH_ASSOC);

			if ($stmt->execute($paramArray)){
				return  $stmt->fetch();
			}else{
				return null;
			}
			
		} catch(PDOException $e) {
			echo 'ERROR: ' . DB_ERROR . $e->getMessage();
		}
	}

	
	 public function executeSave($query, $arrayValues) {
		try {
				
			$dsn = "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=" . CHAR_SET;
			$opt = [ PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_EMULATE_PREPARES => false  , PDO::ATTR_PERSISTENT => true ];
			$pdo = new PDO ( $dsn, DB_USER, DB_PASSWORD, $opt );
				
			$stmt = $pdo->prepare($query);
			$stmt->execute($arrayValues);
			
			/*
			 print '$query : ' . $query;
			 print_r ($paramArray);
			*/
			
			return $pdo->lastInsertId();
				
		} catch(PDOException $e) {
			echo 'ERROR: ' . DB_ERROR . $e->getMessage();
		}
	}	
}

?>