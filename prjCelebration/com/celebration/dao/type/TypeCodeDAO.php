<?php

include_once ('com/celebration/service/address/AddressService.php');

class TypeCodeDAO {

	private static $instance = null;

	private function __construct() {
	}

	static public function getInstance() {
		if (self::$instance === null) {
			self::$instance = new self ();
		}
		return self::$instance;
	}

	public function saveServiceTypeCode(ServiceTypeCode $typCode){

		$insertQuery =prepareQuery(ServiceTypeCode::TYP_COD_TBL_NAM,ServiceTypeCode::SRVS_TYP_DESC);
		$arrayValues = array (
				$typCode->getTypeCodeDescription(),
				$typCode->getCreatedBy(),
				$typCode->getUpdatedBy(),
				$typCode->getVersion (),
				$typCode->getIsActive ()
		);
		
		$result = DBConnection::getInstance()->executeSave ( $insertQuery, $arrayValues );
	}
	
	/**
	 * Common function to prepare type code query for the passing argument.
	 * @param unknown $tableName
	 * @param unknown $descriptionColName
	 * @param unknown $descriptionValue
	 * @return string
	 */
	private function prepareQuery($tableName, $descriptionColName, $descriptionValue){
		return "INSERT INTO " . $tableName . " (" . $descriptionColName . ", CREATE_BY, UPDATE_BY, VER, ACTIVE)  VALUES (?, ?, ?, ?) ";
	}
}
?>