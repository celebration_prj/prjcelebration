<?php

include_once 'com/celebration/domain/UserProfile.php';
include_once 'com/celebration/domain/User.php';

class UserProfileTransformer{
		
		private static $instance = null;
	
		private function __construct() { }
	
		static public function getInstance() {
	
			if (self::$instance === null) {
				self::$instance = new self ();
			}
			return self::$instance;
		}
	
	
	public function loadAddressResultSet($result){
		
		$userProfile = new UserProfile();
		
		$userProfile->setBusinessFName ( $result [UserProfile::BUS_PRFL_FNAME] );
		$userProfile->setBusinessLName ( $result [UserProfile::BUS_PRFL_LNAME] );
		$userProfile->setBusinessName ( $result [UserProfile::BUS_PRFL_NAME] );
		$userProfile->setBusinessWebsite ( $result [UserProfile::BUS_PRFL_WEB] );
		$userProfile->setBusinessEmail ( $result [UserProfile::BUS_PRFL_EMAIL] );
		$userProfile->setBusinessPhone ( $result [UserProfile::BUS_PRFL_PHONE] );
		$userProfile->setCreatedBy ( $result [UserProfile::CREATE_BY] );
		$userProfile->setCreatedDate ( $result [UserProfile::CREAT_DTE] );
		$userProfile->setUpdatedBy ( $result [UserProfile::UPDATE_BY] );
		$userProfile->setUpdatedDate ( $result [UserProfile::UPDATE_DTE] );
		$userProfile->setVersion ( $result [UserProfile::VER] );
		$userProfile->setIsActive ($result [UserProfile::ACTIVE]);
		
		$user = new User();
		$user->setId( $result [UserProfile::USR_ID]);
		$userProfile->setUser ($user);
		
		return $userProfile;
		
	}
	
}
?>