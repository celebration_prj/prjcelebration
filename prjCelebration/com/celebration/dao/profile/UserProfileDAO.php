<?php

class UserProfileDAO {
	
	static private $instance = null;
	
	private function __construct() {}
	
	static public function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	public function getUserProfileByUserId($userId){
		
		$userProfile = new UserProfile();
		$result = DBConnection::getInstance()->executeSQLQuery ( "SELECT * FROM BUS_PRFL WHERE BUS_ID = ? " ,
				array($userId));
		
		if(is_array($result)){
			$userProfile = UserProfileTransformer::getInstance()->loadAddressResultSet($result);
		}
		
		return $userProfile;
		
	}
	public function getUserProfileByUserProfileId($userProfileId){

		$userProfile = new UserProfile();
		$result = DBConnection::getInstance()->executeSQLQuery ( "SELECT * FROM BUS_PRFL WHERE BUS_PRFL_ID = ? " ,
				array($userProfileId));
		
		if(is_array($result)){
			$userProfile = UserProfileTransformer::getInstance()->loadAddressResultSet($result);
		}
		
		return $userProfile;
		
	}
	
	/**
	 * Prepare User statement and execute the query to persist user information.
	 * @param User $user
	 */
	public function save(UserProfile $userProfile){
		
		$insertQuery = " INSERT INTO BUS_PRFL( BUS_PRFL_FNAME,BUS_PRFL_LNAME,BUS_PRFL_NAME, USR_ID, BUS_PRFL_WEB, BUS_PRFL_EMAIL, BUS_PRFL_PHONE, CREATE_BY, CREAT_DTE, UPDATE_BY, " .
				" UPDATE_DTE, VER, ACTIVE) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$arrayValues = array (
				$userProfile->getBusinessFName(),
				$userProfile->getBusinessLName(),
				$userProfile->getBusinessName(),
				$userProfile->getUser()->getId(),
				$userProfile->getBusinessWebsite(),
				$userProfile->getBusinessEmail(),
				$userProfile->getBusinessPhone(),
				$userProfile->getCreatedBy (),
				$userProfile->getCreatedDate(),
				$userProfile->getUpdatedBy (),
				$userProfile->getUpdatedDate(),
				$userProfile->getVersion (),
				$userProfile->getIsActive ()
		);
		return DBConnection::getInstance()->executeSave ( $insertQuery, $arrayValues );
	}
	
	
	public function update(UserProfile $userProfile){
		
		$insertQuery =
		" UPDATE BUS_PRFL SET " .
		" BUS_PRFL_FNAME = ? , " .
		" BUS_PRFL_LNAME = ? , " .
		" BUS_PRFL_NAME = ? , " .
		" USR_ID = ? , " .
		" BUS_PRFL_WEB = ? , " .
		" BUS_PRFL_EMAIL = ? , " .
		" BUS_PRFL_PHONE = ? , " .
		" UPDATE_BY = ? , " .
		" VER = ?  "  .
		" WHERE BUS_PRFL_ID = ? ";
		
		$arrayValues = array (
				$userProfile->getBusinessFName(),
				$userProfile->getBusinessLName(),
				$userProfile->getBusinessName(),
				$userProfile->getUser()->getId(),
				$userProfile->getBusinessWebsite(),
				$userProfile->getBusinessEmail(),
				$userProfile->getBusinessPhone(),
				$userProfile->getUpdatedBy (),
				$userProfile->getVersion () ,
				$userProfile->getId()
		);
				
		return DBConnection::getInstance()->executeSave ( $insertQuery, $arrayValues );
	}

}
?>