<?php

include_once ('com/celebration/service/address/AddressService.php');

class AddressDAO {

	private static $instance = null;

	private function __construct() {
	}

	static public function getInstance() {
		if (self::$instance === null) {
			self::$instance = new self ();
		}
		return self::$instance;
	}
	
	public function update(Address $address){
	
		$sqlUpdate = " UPDATE ADDRESS SET " .
		" ADD_LINE_1 = ?  " .
		", ADD_LINE_2 = ?  " .
		", CITY = ?  " .
		", STATE = ?  " .
		", ZIP = ?  " .
		", CNTRY = ?  " .
		", UPDATE_BY = ?  " .
		", VER = ?  " .
		" WHERE BUS_PRFL_ID = ? " ;
		
		$arrayValues = array (
				$address->getAddressLine1(),
				$address->getAddressLine2(),
				$address->getCity(),
				$address->getState(),
				$address->getZip(),
				$address->getCountry(),
				$address->getUpdatedBy(),
				$address->getVersion (),
				$address->getUserProfile()->getId()
		);
				
		return DBConnection::getInstance()->executeSave ( $sqlUpdate, $arrayValues );
	}
	
	public function save(Address $address){
		
		$insertQuery = "INSERT INTO ADDRESS( BUS_PRFL_ID, ADD_LINE_1, ADD_LINE_2, CITY, STATE, ZIP, CNTRY, CREATE_BY, UPDATE_BY, ACTIVE, VER) " .
					   " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
		$arrayValues = array (
				$address->getUserProfile()->getId(),
				$address->getAddressLine1(),
				$address->getAddressLine2(),
				$address->getCity(),
				$address->getState(),
				$address->getZip(),
				$address->getCountry(),
				$address->getCreatedBy(),
				$address->getUpdatedBy(),
				$address->getIsActive (),
				$address->getVersion ()
		);
		return DBConnection::getInstance()->executeSave ( $insertQuery, $arrayValues );
	}
}
?>