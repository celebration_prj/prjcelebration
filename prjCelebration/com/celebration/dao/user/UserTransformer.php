<?php

class UserTransformer{
		
		private static $instance = null;
	
		private function __construct() { }
	
		static public function getInstance() {
	
			if (self::$instance === null) {
				self::$instance = new self ();
			}
			return self::$instance;
		}
	
	
	public function loadUserFromResultSet($result){
				
		$user = new User ();
		
		if(!isset($result) or is_null($result)){
			return $user;	
		}
		
		$user->setId ( $result [User::USR_ID] );
		$user->setFname ( $result [User::USR_FNAME] );
		$user->setLname ( $result [User::USR_LNAME] );
		$user->setPersonalEmail ( $result [User::EMAIL] );
		$user->setPersonalPhone ( $result [User::PHONE] );
		$user->setUserPassword ( $result [User::USR_PSWD] );
		$user->setUserName ( $result [User::USR_NAME] );
		// $user->setUserType($result[User::USR_FNAME]);
		// $user->setCreatedDate ( $result [User::CREAT_DTE] );
		//$user->setUpdatedDate ( $result [User::UPDATE_DTE] );
		$user->setVersion ( $result [User::VER] );
		$user->setCreatedBy ( $result [User::CREATE_BY] );
		
		$user->setUpdatedBy ( $result [User::UPDATE_BY] );
		
		$user->setIsActive ( $result [User::ACTIVE] );
		
		return $user;
		
	}
	
}
?>