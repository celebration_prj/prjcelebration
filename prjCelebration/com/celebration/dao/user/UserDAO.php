<?php

include_once ('com/celebration/dao/db/DBConnection.php');
include_once ('com/celebration/dao/user/UserTransformer.php');

class UserDAO {
	
	private static $instance = null;
	
	private function __construct() {
	}
	
	static public function getInstance() {
		if (self::$instance === null) {
			self::$instance = new self ();
		}
		return self::$instance;
	}
	
	public function getUserByEmailOrUserName($userEmail , $userName){
				
		$user = new User ();
		$result =  DBConnection::getInstance()->executeSQLQuery ( "SELECT * FROM USR WHERE EMAIL = ? OR USR_NAME = ? " ,
				array($userEmail, $userName));
		
		if(is_array($result)){
			$user = UserTransformer::getInstance()->loadUserFromResultSet($result);
		}
		
		return $user;
	}

	/**
	 * Get User By User Name And Activation Code.
	 * 
	 * @param unknown $userName
	 * @param unknown $activationCode
	 * @return User|unknown
	 */
	public function getUserByUserNameAndActivationCode(User $userReq){
	
		$user = new User ();
		$result =  DBConnection::getInstance()->executeSQLQuery ( "SELECT * FROM USR WHERE USR_NAME = ? and ACT_CODE = ? " ,
				array($userReq->getUserName(), $userReq->getActivationCode()));
	
		if(is_array($result)){
			$user = UserTransformer::getInstance()->loadUserFromResultSet($result);
		}

		return $user;
	}
	
	
	public function validateUserLogin(User $userParam){
		
		$user = new User ();
		$result =  DBConnection::getInstance()->executeSQLQuery ( "SELECT * FROM USR WHERE USR_NAME = ? and USR_PSWD = ? and ACTIVE = ? " , 
				array($userParam->getUserName(), md5($userParam->getUserPassword()),1));
		
		if(is_array($result)){
			$user = UserTransformer::getInstance()->loadUserFromResultSet($result);
		}
		
		return $user;
	}
	
	public function getUser($paramArray) {
		
		$user = new User ();
		$result = DBConnection::getInstance()->executeSQLQuery ( "SELECT * FROM USR WHERE USR_ID = ? " , $paramArray);
		$user = UserTransformer::getInstance()->loadUserFromResultSet($result);
		
		return $user;
	}

	/**
	 * Prepare User statement and execute the query to persist user information.
	 * @param User $user
	 */
	public function update(User $user) {
	
		$updatetQuery = " UPDATE USR SET " .
				" USR_FNAME = ? , " .
				" USR_LNAME = ? , " .
				" EMAIL = ? , " .
				" PHONE = ? , " .
				" VER = ?  " .
				" WHERE USR_ID = ? " ;
		$arrayValues = array (
				$user->getFname (),
				$user->getLname (),
				$user->getPersonalEmail (),
				$user->getPersonalPhone (),
				$user->getVersion (),
				$user->getId()
		);
	
		return  DBConnection::getInstance()->executeSave ( $updatetQuery, $arrayValues );
	}
	
	/**
	 * Prepare User statement and execute the query to persist user information.
	 * @param User $user
	 */
	public function activateUser(User $user) {
		
		$updatetQuery = " UPDATE USR SET " .
				" ACTIVE = ? , " .
				" ACT_CODE = ?  " .
				" WHERE USR_ID = ? " ;
		$arrayValues = array (
				"1",
				"",
				$user->getId()
		);
		
		return  DBConnection::getInstance()->executeSave ( $updatetQuery, $arrayValues );
	}
	
	public function save(User $user) {
		
		$insertQuery = " INSERT INTO USR( USR_FNAME, USR_LNAME, USR_NAME, USR_PSWD, CREATE_BY, UPDATE_BY, " .
				" VER, ACTIVE, EMAIL, PHONE, ACT_CODE) VALUES (? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? )";
		$arrayValues = array (
				$user->getFname (),
				$user->getLname (),
				$user->getUserName (),
				md5($user->getUserPassword()),
				$user->getCreatedBy (),
				$user->getUpdatedBy (),
				$user->getVersion (),
				$user->getIsActive (),
				$user->getPersonalEmail (),
				$user->getPersonalPhone (),
				$user->getActivationCode()
		);
		// Return updated ID
		$result = DBConnection::getInstance()->executeSave ( $insertQuery, $arrayValues );
		
	}
	
	public function deleteUser(User $user) { }
}
?>