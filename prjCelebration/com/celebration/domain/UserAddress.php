<?php


class UserAddress extends BaseClass{
	
	private  $addressLine1;
	private  $addressLine2;
	private  $city;
	private  $state;
	private  $zip;
	
    /**
     * addressLine1
     * @return unkown
     */
    public function getAddressLine1(){
        return $this->addressLine1;
    }

    /**
     * addressLine1
     * @param unkown $addressLine1
     * @return UserAddress
     */
    public function setAddressLine1($addressLine1){
        $this->addressLine1 = $addressLine1;
        return $this;
    }

    /**
     * addressLine2
     * @return unkown
     */
    public function getAddressLine2(){
        return $this->addressLine2;
    }

    /**
     * addressLine2
     * @param unkown $addressLine2
     * @return UserAddress
     */
    public function setAddressLine2($addressLine2){
        $this->addressLine2 = $addressLine2;
        return $this;
    }

    /**
     * city
     * @return unkown
     */
    public function getCity(){
        return $this->city;
    }

    /**
     * city
     * @param unkown $city
     * @return UserAddress
     */
    public function setCity($city){
        $this->city = $city;
        return $this;
    }

    /**
     * state
     * @return unkown
     */
    public function getState(){
        return $this->state;
    }

    /**
     * state
     * @param unkown $state
     * @return UserAddress
     */
    public function setState($state){
        $this->state = $state;
        return $this;
    }

    /**
     * zip
     * @return unkown
     */
    public function getZip(){
        return $this->zip;
    }

    /**
     * zip
     * @param unkown $zip
     * @return UserAddress
     */
    public function setZip($zip){
        $this->zip = $zip;
        return $this;
    }

}


?>