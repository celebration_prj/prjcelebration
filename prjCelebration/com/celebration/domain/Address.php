<?php

class Address extends BaseClass {
	
	const BUS_PRFL_ID 	= "BUS_PRFL_ID" ;
	const ADD_ID 		= "ADD_ID" ;
	const ADD_LINE_1 	= "ADD_LINE_1" ;
	const ADD_LINE_2 	= "ADD_LINE_2";
	const CITY 			= "CITY";
	const STATE 		= "STATE";
	const ZIP 			= "ZIP";
	const CNTRY 		= "CNTRY";
	
	private $userProfile;
	private $addressLine1;
	private $addressLine2;
	private $city;
	private $state;
	private $zip;
	private $country;
	
	
	function __construct() {}
	
	/**
	 * addressLine1
	 * 
	 * @return unkown
	 */
	public function getAddressLine1() {
		return $this->addressLine1;
	}
	
	/**
	 * addressLine1
	 * 
	 * @param unkown $addressLine1        	
	 * @return Address
	 */
	public function setAddressLine1($addressLine1) {
		$this->addressLine1 = $addressLine1;
		return $this;
	}
	
	/**
	 * addressLine2
	 * 
	 * @return unkown
	 */
	public function getAddressLine2() {
		return $this->addressLine2;
	}
	
	/**
	 * addressLine2
	 * 
	 * @param unkown $addressLine2        	
	 * @return Address
	 */
	public function setAddressLine2($addressLine2) {
		$this->addressLine2 = $addressLine2;
		return $this;
	}
	
	/**
	 * city
	 * 
	 * @return unkown
	 */
	public function getCity() {
		return $this->city;
	}
	
	/**
	 * city
	 * 
	 * @param unkown $city        	
	 * @return Address
	 */
	public function setCity($city) {
		$this->city = $city;
		return $this;
	}
	
	/**
	 * state
	 * 
	 * @return unkown
	 */
	public function getState() {
		return $this->state;
	}
	
	/**
	 * state
	 * 
	 * @param unkown $state        	
	 * @return Address
	 */
	public function setState($state) {
		$this->state = $state;
		return $this;
	}
	
	/**
	 * zip
	 * 
	 * @return unkown
	 */
	public function getZip() {
		return $this->zip;
	}
	
	/**
	 * zip
	 * 
	 * @param unkown $zip        	
	 * @return Address
	 */
	public function setZip($zip) {
		$this->zip = $zip;
		return $this;
	}
	
	/**
	 * country
	 * 
	 * @return unkown
	 */
	public function getCountry() {
		return $this->country;
	}
	
	/**
	 * country
	 * 
	 * @param unkown $country        	
	 * @return Address
	 */
	public function setCountry($country) {
		$this->country = $country;
		return $this;
	}
	


    /**
     * userProfile
     * @return unkown
     */
    public function getUserProfile(){
        return $this->userProfile;
    }

    /**
     * userProfile
     * @param unkown $userProfile
     * @return Address
     */
    public function setUserProfile(UserProfile $userProfile){
        $this->userProfile = $userProfile;
        return $this;
    }

}
?>