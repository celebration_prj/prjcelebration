<?php

class ResponseEntity{
	
	 public $errorMessages = array();
	 public $warningMessages = array();
	 public $entity;
	
	 
	 public function hasError(){
	 	return count($this->errorMessages) > 0;
	 }
	 
	 public function hasWarning(){
	 	return count($this->warningMessages) > 0;
	 }
    /**
     * errorMessages
     * @return unkown
     */
    public function getErrorMessages(){
        return $this->errorMessages;
    }

    /**
     * errorMessages
     * @param unkown $errorMessages
     * @return ResponseEntity{
     */
    public function setErrorMessages($errorMessages){
        $this->errorMessages = $errorMessages;
        return $this;
    }

    /**
     * warningMessages
     * @return unkown
     */
    public function getWarningMessages(){
        return $this->warningMessages;
    }

    /**
     * warningMessages
     * @param unkown $warningMessages
     * @return ResponseEntity{
     */
    public function setWarningMessages($warningMessages){
        $this->warningMessages = $warningMessages;
        return $this;
    }

    /**
     * entity
     * @return unkown
     */
    public function getEntity(){
        return $this->entity;
    }

    /**
     * entity
     * @param unkown $entity
     * @return ResponseEntity{
     */
    public function setEntity($entity){
        $this->entity = $entity;
        return $this;
    }

}
?>