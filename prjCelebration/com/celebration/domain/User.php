<?php

include_once 'com/celebration/domain/BaseClass.php';

class User extends BaseClass{
	
	const PHONE     =  "PHONE" ;
	const EMAIL     =  "EMAIL" ;
	const USR_ID    =  "USR_ID" ;
	const USR_NAME  =  "USR_NAME" ;
	const USR_PSWD  =  "USR_PSWD" ;
	const USR_LNAME =  "USR_LNAME" ;
	const USR_FNAME =  "USR_FNAME" ;
	const ACT_CODE  =  "ACT_CODE" ;
	
	//strtoupper(uniqid())
	
	
	private  $fname;
	private  $lname;
	private  $userType;
	private  $personalEmail;
	private  $personalPhone;
	private  $userName;
	private  $userPassword;
	private  $activationCode;
	
	
    /**
     * fname
     * @return unkown
     */
    public function getFname(){
        return $this->fname;
    }

    /**
     * fname
     * @param unkown $fname
     * @return User
     */
    public function setFname($fname){
        $this->fname = $fname;
        return $this;
    }

    /**
     * lname
     * @return unkown
     */
    public function getLname(){
        return $this->lname;
    }

    /**
     * lname
     * @param unkown $lname
     * @return User
     */
    public function setLname($lname){
        $this->lname = $lname;
        return $this;
    }

    /**
     * userType
     * @return unkown
     */
    public function getUserType(){
        return $this->userType;
    }

    /**
     * userType
     * @param unkown $userType
     * @return User
     */
    public function setUserType($userType){
        $this->userType = $userType;
        return $this;
    }

    /**
     * userName
     * @return unkown
     */
    public function getUserName(){
        return $this->userName;
    }

    /**
     * userName
     * @param unkown $userName
     * @return User
     */
    public function setUserName($userName){
        $this->userName = $userName;
        return $this;
    }

    /**
     * userPassword
     * @return unkown
     */
    public function getUserPassword(){
        return $this->userPassword;
    }

    /**
     * userPassword
     * @param unkown $userPassword
     * @return User
     */
    public function setUserPassword($userPassword){
        $this->userPassword = $userPassword;
        return $this;
    }


    /**
     * personalEmail
     * @return unkown
     */
    public function getPersonalEmail(){
        return $this->personalEmail;
    }

    /**
     * personalEmail
     * @param unkown $personalEmail
     * @return User
     */
    public function setPersonalEmail($personalEmail){
        $this->personalEmail = $personalEmail;
        return $this;
    }


    /**
     * personalPhone
     * @return unkown
     */
    public function getPersonalPhone(){
        return $this->personalPhone;
    }

    /**
     * personalPhone
     * @param unkown $personalPhone
     * @return User
     */
    public function setPersonalPhone($personalPhone){
        $this->personalPhone = $personalPhone;
        return $this;
    }
    

    public function getActivationCode(){
        return $this->activationCode;
    }

    public function setActivationCode($activationCode){
        $this->activationCode = $activationCode;
        return $this;
    }

}
?>