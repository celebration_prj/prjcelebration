<?php

class BaseClass {
	protected $id;
	protected $version;
	protected $createdDate;
	protected $createdBy;
	protected $updatedDate;
	protected $updatedBy;
	protected $isActive;
	
	const VER = "VER";
	const ACTIVE = "ACTIVE";
	const CREATE_BY = "CREATE_BY";
	const UPDATE_BY = "UPDATE_BY";
	const CREAT_DTE = "CREAT_DTE";
	const UPDATE_DTE = "UPDATE_DTE";

    /**
     * id
     * @return unkown
     */
    public function getId(){
        return $this->id;
    }

    /**
     * id
     * @param unkown $id
     * @return BaseClass
     */
    public function setId($id){
        $this->id = $id;
        return $this;
    }

    /**
     * version
     * @return unkown
     */
    public function getVersion(){
        return $this->version;
    }

    /**
     * version
     * @param unkown $version
     * @return BaseClass
     */
    public function setVersion($version){
        $this->version = $version;
        return $this;
    }

    /**
     * createdDate
     * @return unkown
     */
    public function getCreatedDate(){
        return $this->createdDate;
    }

    /**
     * createdDate
     * @param unkown $createdDate
     * @return BaseClass
     */
    public function setCreatedDate(Date $createdDate){
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * createdBy
     * @return unkown
     */
    public function getCreatedBy(){
        return $this->createdBy;
    }

    /**
     * createdBy
     * @param unkown $createdBy
     * @return BaseClass
     */
    public function setCreatedBy($createdBy){
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * updatedDate
     * @return unkown
     */
    public function getUpdatedDate(){
        return $this->updatedDate;
    }

    /**
     * updatedDate
     * @param unkown $updatedDate
     * @return BaseClass
     */
    public function setUpdatedDate(Date $updatedDate){
        $this->updatedDate = $updatedDate;
        return $this;
    }

    /**
     * updatedBy
     * @return unkown
     */
    public function getUpdatedBy(){
        return $this->updatedBy;
    }

    /**
     * updatedBy
     * @param unkown $updatedBy
     * @return BaseClass
     */
    public function setUpdatedBy($updatedBy){
        $this->updatedBy = $updatedBy;
        return $this;
    }

    /**
     * isActive
     * @return unkown
     */
    public function getIsActive(){
        return $this->isActive;
    }

    /**
     * isActive
     * @param unkown $isActive
     * @return BaseClass
     */
    public function setIsActive($isActive){
        $this->isActive = $isActive;
        return $this;
    }
    
    public function hasValidRecord() {
    	if(empty($this->getId()) or is_nan($this->getId())){
    		return FALSE;
    	}
    
    	return TRUE;
    }
    
    public function __toString() {
    	return "" . $this->getId();
    }
    
    public function loadCreatedByDefault(){
    	$this->setCreatedBy("ADMIN");
    	$this->setCreatedDate(new DateTime('NOW'));
    }
    
    public function loadUpdatedByDefault(){
    	$this->setUpdatedBy("ADMIN");
    	$this->setUpdatedDate(new DateTime('NOW'));
    }

    public function loadDefaultVersionAndActiveIndicator(){
    	$this->setIsActive(1);
    	$this->setVersion(0);
    }
}

?>