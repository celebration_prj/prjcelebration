<?php

class SearchCriteria{
	
	private $userId;
	private $userProfileId;
	private $addressId;
	private $fname;
	private $lname;
	private $businessName;
	private $businessWebsite;
	private $city;
	private $state;
	private $zip;
	private $country;



    public function getUserId(){
        return $this->userId;
    }

    public function setUserId($userId){
        $this->userId = $userId;
        return $this;
    }

    public function getUserProfileId(){
        return $this->userProfileId;
    }

    public function setUserProfileId($userProfileId){
        $this->userProfileId = $userProfileId;
        return $this;
    }

    public function getAddressId(){
        return $this->addressId;
    }

    public function setAddressId($addressId){
        $this->addressId = $addressId;
        return $this;
    }

    public function getFname(){
        return $this->fname;
    }

    public function setFname($fname){
        $this->fname = $fname;
        return $this;
    }

    public function getLname(){
        return $this->lname;
    }

    public function setLname($lname){
        $this->lname = $lname;
        return $this;
    }

    public function getBusinessName(){
        return $this->businessName;
    }

    public function setBusinessName($businessName){
        $this->businessName = $businessName;
        return $this;
    }

    public function getBusinessWebsite(){
        return $this->businessWebsite;
    }

    public function setBusinessWebsite($businessWebsite){
        $this->businessWebsite = $businessWebsite;
        return $this;
    }

    public function getCity(){
        return $this->city;
    }

    public function setCity($city){
        $this->city = $city;
        return $this;
    }

    public function getState(){
        return $this->state;
    }

    public function setState($state){
        $this->state = $state;
        return $this;
    }

    public function getZip(){
        return $this->zip;
    }

    public function setZip($zip){
        $this->zip = $zip;
        return $this;
    }

    public function getCountry(){
        return $this->country;
    }

    public function setCountry($country){
        $this->country = $country;
        return $this;
    }

}
?>