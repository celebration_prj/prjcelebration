<?php

class SearchResult {


	const USR_ID 		= "USR_ID" ;
	const PHONE 		=  "PHONE" ;
	const EMAIL 		=  "EMAIL" ;
	const USR_NAME 		=  "USR_NAME" ;
	const USR_PSWD 		=  "USR_PSWD" ;
	const USR_LNAME 	=  "USR_LNAME" ;
	const USR_FNAME 	=  "USR_FNAME" ;
	
	const BUS_PRFL_ID 		= "BUS_PRFL_ID" ;
	const BUS_PRFL_FNAME 	= "BUS_PRFL_FNAME";
	const BUS_PRFL_LNAME	= "BUS_PRFL_LNAME";
	const BUS_PRFL_NAME 	= "BUS_PRFL_NAME";
	const BUS_PRFL_WEB 		= "BUS_PRFL_WEB";
	const BUS_PRFL_EMAIL 	= "BUS_PRFL_EMAIL";
	const BUS_PRFL_PHONE 	= "BUS_PRFL_PHONE";
	
	const ADD_ID 		= "ADD_ID" ;
	const ADD_LINE_1 	= "ADD_LINE_1" ;
	const ADD_LINE_2 	= "ADD_LINE_2";
	const CITY 			= "CITY";
	const STATE 		= "STATE";
	const ZIP 			= "ZIP";
	const CNTRY 		= "CNTRY";
	
	/* User Related Information */
	private  $userId;
	private  $fname;
	private  $lname;
	private  $userType;
	private  $personalEmail;
	private  $personalPhone;
	private  $userName;
	private  $userPassword;

	/* Business Profile Related Fields*/
	private $businessProfileId;
	private $businessFName;
	private $businessLName;
	private $businessName;
	private $businessWebsite;
	private $businessPhone;
	private $businessEmail;
	
	/* Address Related Fields */
	private $addressId;
	private $addressLine1;
	private $addressLine2;
	private $city;
	private $state;
	private $zip;
	private $country;

	
    public function getFname(){
        return $this->fname;
    }

    public function setFname($fname){
        $this->fname = $fname;
        return $this;
    }

    public function getLname(){
        return $this->lname;
    }

    public function setLname($lname){
        $this->lname = $lname;
        return $this;
    }

    public function getUserType(){
        return $this->userType;
    }

    public function setUserType($userType){
        $this->userType = $userType;
        return $this;
    }

    public function getPersonalEmail(){
        return $this->personalEmail;
    }

    public function setPersonalEmail($personalEmail){
        $this->personalEmail = $personalEmail;
        return $this;
    }

    public function getPersonalPhone(){
        return $this->personalPhone;
    }

    public function setPersonalPhone($personalPhone){
        $this->personalPhone = $personalPhone;
        return $this;
    }

    public function getUserName(){
        return $this->userName;
    }

    public function setUserName($userName){
        $this->userName = $userName;
        return $this;
    }

    public function getUserPassword(){
        return $this->userPassword;
    }

    public function setUserPassword($userPassword){
        $this->userPassword = $userPassword;
        return $this;
    }

    public function getBusinessFName(){
        return $this->businessFName;
    }

    public function setBusinessFName($businessFName){
        $this->businessFName = $businessFName;
        return $this;
    }

    public function getBusinessLName(){
        return $this->businessLName;
    }

    public function setBusinessLName($businessLName){
        $this->businessLName = $businessLName;
        return $this;
    }

    public function getBusinessName(){
        return $this->businessName;
    }

    public function setBusinessName($businessName){
        $this->businessName = $businessName;
        return $this;
    }

    public function getBusinessWebsite(){
        return $this->businessWebsite;
    }

    public function setBusinessWebsite($businessWebsite){
        $this->businessWebsite = $businessWebsite;
        return $this;
    }

    public function getBusinessPhone(){
        return $this->businessPhone;
    }

    public function setBusinessPhone($businessPhone){
        $this->businessPhone = $businessPhone;
        return $this;
    }

    public function getBusinessEmail(){
        return $this->businessEmail;
    }

    public function setBusinessEmail($businessEmail){
        $this->businessEmail = $businessEmail;
        return $this;
    }

    public function getAddressLine1(){
        return $this->addressLine1;
    }

    public function setAddressLine1($addressLine1){
        $this->addressLine1 = $addressLine1;
        return $this;
    }

    public function getAddressLine2(){
        return $this->addressLine2;
    }

    public function setAddressLine2($addressLine2){
        $this->addressLine2 = $addressLine2;
        return $this;
    }

    public function getCity(){
        return $this->city;
    }

    public function setCity($city){
        $this->city = $city;
        return $this;
    }

    public function getState(){
        return $this->state;
    }

    public function setState($state){
        $this->state = $state;
        return $this;
    }

    public function getZip(){
        return $this->zip;
    }

    public function setZip($zip){
        $this->zip = $zip;
        return $this;
    }

    public function getCountry(){
        return $this->country;
    }

    public function setCountry($country){
        $this->country = $country;
        return $this;
    }


    public function getAddressId(){
        return $this->addressId;
    }

    public function setAddressId($addressId){
        $this->addressId = $addressId;
        return $this;
    }


    public function getBusinessProfileId(){
        return $this->businessProfileId;
    }

    public function setBusinessProfileId($businessProfileId){
        $this->businessProfileId = $businessProfileId;
        return $this;
    }
    

    public function getUserId(){
        return $this->userId;
    }

    public function setUserId($userId){
        $this->userId = $userId;
        return $this;
    }

}
?>