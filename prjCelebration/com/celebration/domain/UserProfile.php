<?php
include 'com/celebration/domain/User.php';

class UserProfile extends BaseClass {
	
	const BUS_PRFL_ID = "BUS_PRFL_ID" ;
	const USR_ID = "USR_ID" ;
	const BUS_PRFL_FNAME = "BUS_PRFL_FNAME";
	const BUS_PRFL_LNAME = "BUS_PRFL_LNAME";
	const BUS_PRFL_NAME = "BUS_PRFL_NAME";
	const BUS_PRFL_WEB = "BUS_PRFL_WEB";
	const BUS_PRFL_EMAIL = "BUS_PRFL_EMAIL";
	const BUS_PRFL_PHONE = "BUS_PRFL_PHONE";
	
	
	private $businessFName;
	private $businessLName;
	private $businessName;
	private $businessWebsite;
	private $businessPhone;
	private $businessEmail;
	private $address;
	private $user;
	
	function __construct() {
	}
	
	/**
	 * businessName
	 * 
	 * @return unkown
	 */
	public function getBusinessName() {
		return $this->businessName;
	}
	
	/**
	 * businessName
	 * 
	 * @param unkown $businessName        	
	 * @return UserProfile
	 */
	public function setBusinessName($businessName) {
		$this->businessName = $businessName;
		return $this;
	}
	
	/**
	 * businessWebsite
	 * 
	 * @return unkown
	 */
	public function getBusinessWebsite() {
		return $this->businessWebsite;
	}
	
	/**
	 * businessWebsite
	 * 
	 * @param unkown $businessWebsite        	
	 * @return UserProfile
	 */
	public function setBusinessWebsite($businessWebsite) {
		$this->businessWebsite = $businessWebsite;
		return $this;
	}
	
	/**
	 * businessPhone
	 * 
	 * @return unkown
	 */
	public function getBusinessPhone() {
		return $this->businessPhone;
	}
	
	/**
	 * businessPhone
	 * 
	 * @param unkown $businessPhone        	
	 * @return UserProfile
	 */
	public function setBusinessPhone($businessPhone) {
		$this->businessPhone = $businessPhone;
		return $this;
	}
	
	/**
	 * businessEmail
	 * 
	 * @return unkown
	 */
	public function getBusinessEmail() {
		return $this->businessEmail;
	}
	
	/**
	 * businessEmail
	 * 
	 * @param unkown $businessEmail        	
	 * @return UserProfile
	 */
	public function setBusinessEmail($businessEmail) {
		$this->businessEmail = $businessEmail;
		return $this;
	}
	
	/**
	 * address
	 * 
	 * @return unkown
	 */
	public function getAddress() {
		return $this->address;
	}
	
	/**
	 * address
	 * 
	 * @param unkown $address        	
	 * @return UserProfile
	 */
	public function setAddress($address) {
		$this->address = $address;
		return $this;
	}
	
	/**
	 * user
	 * 
	 * @return unkown
	 */
	public function getUser() {
		return $this->user;
	}
	
	/**
	 * user
	 * 
	 * @param unkown $user        	
	 * @return UserProfile
	 */
	public function setUser(User $user) {
		$this->user = $user;
		return $this;
	}
	
	

    /**
     * businessFName
     * @return unkown
     */
    public function getBusinessFName(){
        return $this->businessFName;
    }

    /**
     * businessFName
     * @param unkown $businessFName
     * @return UserProfile
     */
    public function setBusinessFName($businessFName){
        $this->businessFName = $businessFName;
        return $this;
    }

    /**
     * businessLName
     * @return unkown
     */
    public function getBusinessLName(){
        return $this->businessLName;
    }

    /**
     * businessLName
     * @param unkown $businessLName
     * @return UserProfile
     */
    public function setBusinessLName($businessLName){
        $this->businessLName = $businessLName;
        return $this;
    }

}
?>