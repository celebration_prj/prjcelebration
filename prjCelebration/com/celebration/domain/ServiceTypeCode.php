<?php

include_once 'com/celebration/domain/BaseClass.php';

class ServiceTypeCode extends BaseClass{

	const SRVS_TYP_ID =  "SRVS_TYP_ID" ;
	const SRVS_TYP_DESC =  "SRVS_TYP_DESC" ;
	const TYP_COD_TBL_NAM = "SRVS_TYP";

	private  $typeCodeDescription;
	
	

    /**
     * typeCodeDescription
     * @return unkown
     */
    public function getTypeCodeDescription(){
        return $this->typeCodeDescription;
    }

    /**
     * typeCodeDescription
     * @param unkown $typeCodeDescription
     * @return ServiceTypeCode
     */
    public function setTypeCodeDescription($typeCodeDescription){
        $this->typeCodeDescription = $typeCodeDescription;
        return $this;
    }

}
?>
