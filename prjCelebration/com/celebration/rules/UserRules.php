<?php

include ('com/celebration/utility/userProperties.php');

class UserRules{

	/* ==================== Single Instance Logic ============================*/
	static private $instance = null;	
	
	private function __construct() {}
	
	static public function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	/* ========================================================================*/
	
	public function validateLogin(User $user){
		$errorArray = array();

		if(!$user->hasValidRecord()){
			$errorArray[] = ERROR_LOGIN;
		}
		
		return $errorArray;
	}
	
	public function validateUserInformationToLogin(User $user){
		
		$errorArray = array();
		
		if (is_null($user->getUserName()) || empty($user->getUserName())){
			$errorArray[] = ERROR_USER_LOGIN_ID_REQUIRED;
		}
		if (is_null($user->getUserPassword()) || empty($user->getUserPassword())){
			$errorArray[] =  ERROR_USER_LOGIN_PASSWORD_REQUIRED;
		}
		
		return $errorArray;
	}
	
	/**
	 * 
	 * @param User $user
	 * @return string[]
	 */
	public function validateUserToActivate ( User $user){
		
		$errorArray = array();
		
		if (is_null($user->getUserName()) || empty ( $user->getUserName())) {
			
			$errorArray [] = ERROR_USER_ACTIVATION_MISSING;
			
		} else if (is_null($user->getActivationCode()) || empty ( $user->getActivationCode())) {
			
			$errorArray [] = ERROR_USER_ACTIVATION_MISSING;
		}
		
		return $errorArray;
		
	}

	/**
	 *
	 * @param User $dbEntity : Database User Entity
	 * @return string[] : return of error message.
	 */
	public function validateExistingUserToActivate(User $dbEntity){
	
		$errorArray = array();

		if (!$dbEntity->hasValidRecord()) {
			$errorArray[] = ERROR_USER_DOES_NOT_EXIST_TO_ACTIVATE;
		}
		return $errorArray;
	}
	
	
	/**
	 * 
	 * @param User $dbEntity : Database User Entity
	 * @return string[] : return of error message.
	 */
	public function validateExistingUser(User $dbEntity){
		
		$errorArray = array();
		
		if ($dbEntity->hasValidRecord()) {
			$errorArray[] = ERROR_ALREADY_EXIST;
		}
		return $errorArray;
	}
	
	/**
	 * 
	 * @param User $userEntity : Information entered by user.
	 * @return string[] : return array of error message.
	 */
	public function validateNewUserRegistration(User $userEntity){
		
		$errorArray = array();
		
		if ( isset($userEntity)){
			
			if (is_null($userEntity->getFname()) || empty($userEntity->getFname())){
				$errorArray[] = ERROR_USER_FNAME_REQUIRED;
			}
			if (is_null($userEntity->getLname()) || empty($userEntity->getLname())){
				$errorArray[] =  ERROR_USER_LNAME_REQUIRED;
			}
			if (is_null($userEntity->getUserName()) || empty($userEntity->getUserName())){
				$errorArray[] =  ERROR_USER_NAME_REQUIRED;
			}
			if (is_null($userEntity->getUserPassword()) || empty($userEntity->getUserPassword())){
				$errorArray[] =  ERROR_USER_PASSWORD_REQUIRED;
			}
			if (is_null($userEntity->getPersonalEmail()) || empty($userEntity->getPersonalEmail())){
				$errorArray[] =  ERROR_USER_EMAIL_REQUIRED;
			}
			
		}else{
			$errorArray[] = ERROR_INFORMATION_REQUIRED;
		}
		
		return $errorArray;
	}
	
}

?>

