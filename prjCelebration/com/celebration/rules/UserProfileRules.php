<?php

include ('com/celebration/utility/userProfileProperties.php');

class UserRules{


	/* ==================== Single Instance Logic ============================*/
	static private $instance = null;
	
	private function __construct() {}
	
	static public function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	/* ========================================================================*/
}

?>

