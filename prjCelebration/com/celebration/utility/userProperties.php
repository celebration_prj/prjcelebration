<?php

	define ("ERROR_USER_FNAME_REQUIRED" , "User first name is a required field.");
	define ("ERROR_USER_LNAME_REQUIRED" , "User last name is a required field.");
	define ("ERROR_USER_NAME_REQUIRED" , "User name is a required field.");
	define ("ERROR_USER_PASSWORD_REQUIRED" , "User password is a required field.");
	define ("ERROR_USER_EMAIL_REQUIRED" ,"User email is a required field.");
	define ("ERROR_INFORMATION_REQUIRED" ,"User information can not be empty.");
	define ("ERROR_ALREADY_EXIST" ,"User with this email/login id already exist. Please use different information to register.");
	
	/* User activate error*/
	define ("ERROR_USER_ACTIVATION_MISSING" ,"Required information to activate user is missing.");
	define ("ERROR_USER_DOES_NOT_EXIST_TO_ACTIVATE" ,"No user exist to activate. Please check information you provided.");
	
	// User Login validation message
	define ("ERROR_USER_LOGIN_ID_REQUIRED" ,"User login id is required to login.");
	define ("ERROR_USER_LOGIN_PASSWORD_REQUIRED" ,"User login password is required to login.");
	
	define ("ERROR_LOGIN" ,"The user name/password is incorrect or account is not avtivated." );
	
	define ("SUCCESS" ,"sucessPage");
	define ("ERROR" ,"user.php");
	
?>