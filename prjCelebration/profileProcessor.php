<?php


include_once ('com/celebration/domain/UserProfile.php');
include_once ('com/celebration/domain/ResponseEntity.php');
include_once ('com/celebration/controller/UserController.php');
include_once ('com/celebration/controller/AddressController.php');
include_once ('com/celebration/controller/UserProfileController.php');
include_once ('com/celebration/domain/Address.php');


$userProfile = new UserProfile();
$address = new Address();
$user = new User();

session_start();

$userId = $_SESSION[User::USR_ID];
$userProfileId = $_POST[UserProfile::BUS_PRFL_ID];
$addressId =  $_POST [Address::ADD_ID] ;
$userLoginNameFromSesion = $_SESSION[User::USR_NAME];




/* Getting User profile information from session */
$userProfile->setId($userProfileId);
$userProfile->setBusinessFName($_POST[UserProfile::BUS_PRFL_FNAME]);
$userProfile->setBusinessLName($_POST[UserProfile::BUS_PRFL_LNAME]);
$userProfile->setBusinessName($_POST[UserProfile::BUS_PRFL_NAME]);
$userProfile->setBusinessWebsite($_POST[UserProfile::BUS_PRFL_WEB]);
$userProfile->setBusinessEmail($_POST[UserProfile::BUS_PRFL_EMAIL]);
$userProfile->setBusinessPhone($_POST[UserProfile::BUS_PRFL_PHONE]);

$userProfile->setCreatedBy($userLoginNameFromSesion);
$userProfile->setUpdatedBy($userLoginNameFromSesion);
$userProfile->loadDefaultVersionAndActiveIndicator();


/* User Information To be getting to persist.*/
/*-------------------------------------------*/

$user->setId($userId);

$response = new ResponseEntity();
$response =  UserController::getInstance ()->getUser(array($user->getId()));
$userProfile->setUser($user);


/* User Profile Address To be getting to persist.*/
/*-------------------------------------------*/
$address->setId($addressId);
$address->setAddressLine1($_POST[Address::ADD_LINE_1]);
$address->setAddressLine2($_POST[Address::ADD_LINE_2]);
$address->setCity($_POST[Address::CITY]);
$address->setState($_POST[Address::STATE]);
$address->setZip($_POST[Address::ZIP]);
$address->setCountry("Pakistan");
$address->setUserProfile($userProfile);
$address->loadDefaultVersionAndActiveIndicator();



/* To Check If UserProfile already exisit and Do we need to call Update/Save */
if (isset ( $userProfileId ) && ! empty ( $userProfileId ) && $userProfile->hasValidRecord()) {
	$userProfileId = UserProfileController::getInstance()->update($userProfile);
}else{
	$userProfileId = UserProfileController::getInstance()->save($userProfile);
	$userProfile->setId($userProfileId);
}

/* To Check If Address already exisit and Do we need to call Update/Save */
if (isset ( $addressId ) && ! empty ( $addressId ) && $address->hasValidRecord()) {
	AddressController::getInstance()->update($address);
}else{
	AddressController::getInstance()->save($address);
}
	

if ($response->hasError()){
	print_r($response->getErrorMessages());
}else{
	echo "User profile register successfully ..........!";
}
?>
