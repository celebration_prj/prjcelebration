<?php
include_once ('com/celebration/domain/SearchResult.php');
include_once ('com/celebration/domain/SearchCriteria.php');
include_once ('com/celebration/controller/SearchController.php');

$searchResult = new SearchResult();
session_start();

$searchCriteria = new SearchCriteria();
$searchCriteria->setUserId(1);
$searchResult = SearchController::getInstance()->search($searchCriteria);

$_SESSION["SEARCH_RESULT"] = $searchResult;

include 'profile.php';

?>